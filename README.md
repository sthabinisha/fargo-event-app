# Fargo Event App
This app has following packages
API: It contains all the api packages
Utils:data: Utility Classes
Helper: NoInternet and shared preferences helper

Classes has been used in such a way  that it can be inherited and can be reuse
I've decided to follow an approach where the project is splitted into three different modules:

MVP architecture inclues model, presenter and view classes

View: Activity/Fragment will contain a reference to the presenter. 
Presenter:
The presenter is responsible to act as the middleman between view and model. It retrieves data from the model and returns it formatted to the view. It basically  mediates  between model and view


Library reference resources:
ButterKnife: http://jakewharton.github.io/butterknife/
Retrofit: http://square.github.io/retrofit/
AppCompat, CardView, RecyclerView an Design
