package com.myriadmobile.fargoeventsapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.myriadmobile.fargoeventsapp.R;
import com.myriadmobile.fargoeventsapp.base.BaseActivity;
import com.myriadmobile.fargoeventsapp.eventProfile.view.EventProfileFragment;
import com.myriadmobile.fargoeventsapp.eventsList.model.EvenListUserModel;
import com.myriadmobile.fargoeventsapp.eventsList.view.EventListFragment;
import com.myriadmobile.fargoeventsapp.helper.SharedPreferencesHelper;
import com.myriadmobile.fargoeventsapp.login.view.LoginActivity;
import com.myriadmobile.fargoeventsapp.utils.Constant;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.mb_toolbar_title)
    public TextView toolbarTitle;
    @BindView(R.id.main_activity_content_frame)
    FrameLayout frameLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SharedPreferencesHelper sharedPreferencesHelper;


    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void init() {
        setToolbar();
        setToolBarTitle(Constant.EVENTS);
    }

    private void loadFragment() {
        EventListFragment fragment = new EventListFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, fragment, "profile")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    void setToolbar() {
        setSupportActionBar(toolbar);
        sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(this);
        Constant.ACESS_TOKEN = sharedPreferencesHelper.getLoginPreferecnes();
        if (Constant.ACESS_TOKEN.equalsIgnoreCase("")) {
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        } else {
            loadFragment();
        }

    }

    public void setToolBarTitle(String title) {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(title);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_login:

                SharedPreferencesHelper sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(MainActivity.this);
                sharedPreferencesHelper.clearLoginPreferences();
                Intent b = new Intent(MainActivity.this, LoginActivity.class);
                b.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                b.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                b.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(b);
                finish();
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }



}
