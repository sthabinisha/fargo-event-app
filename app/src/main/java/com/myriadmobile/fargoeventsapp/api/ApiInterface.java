package com.myriadmobile.fargoeventsapp.api;


import com.myriadmobile.fargoeventsapp.eventProfile.model.EvenProfileUserModel;
import com.myriadmobile.fargoeventsapp.eventProfile.model.EventSpeakerUserModel;
import com.myriadmobile.fargoeventsapp.eventsList.model.EvenListUserModel;
import com.myriadmobile.fargoeventsapp.login.model.LoginUserModel;
import com.myriadmobile.fargoeventsapp.login.model.UserResponseModel;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface ApiInterface {

    @POST("api/v1/login")
    Call<UserResponseModel> getUserData(@Body LoginUserModel loginUserModel);


    @GET("api/v1/events")
    Call<ArrayList<EvenListUserModel>> getEventData();
//    Call<ResponseBody> getEventData();

    @GET("api/v1/events/{id}")
    Call<EvenProfileUserModel> getEventProfile(@Path("id") String eventID);

    @GET("api/v1/speakers/{id}")
    Call<EventSpeakerUserModel> getSpeakers(@Path("id") String eventID);


}
