package com.myriadmobile.fargoeventsapp.eventsList.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myriadmobile.fargoeventsapp.R;
import com.myriadmobile.fargoeventsapp.eventsList.model.EvenListUserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.support.v7.widget.RecyclerView.ViewHolder;

public class EventListRecyclerViewAdapter extends RecyclerView.Adapter<EventListRecyclerViewAdapter.MyViewHolder> implements AbsListView.OnScrollListener {
    ViewHolder viewHolder;
    CustomItemClickListener listener;
    private Context mContext;
    private ArrayList<EvenListUserModel> evenListUserModels = new ArrayList<>();

    public EventListRecyclerViewAdapter(Context mContext, ArrayList<EvenListUserModel> evenListUserModelArrayList, CustomItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.evenListUserModels = evenListUserModelArrayList;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

        final Picasso picasso = Picasso.with(mContext);

        if (i == SCROLL_STATE_IDLE || i == SCROLL_STATE_TOUCH_SCROLL) {
            picasso.resumeTag(mContext);
        } else {
            picasso.pauseTag(mContext);
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_list, parent, false);
        final MyViewHolder mViewHolder = new MyViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        EvenListUserModel evenListUserModel = evenListUserModels.get(position);
        holder.eventTitle.setText(evenListUserModel.getTitle());
        holder.eventDate.setText(evenListUserModel.getStart_date_time());
        try {
            Picasso.with(mContext)
                    .load(evenListUserModel.getImage_url())
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder)
                    .tag("Profile")
                    .into(holder.eventImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return evenListUserModels.size();
    }

    public interface CustomItemClickListener {
        public void onItemClick(View v, int position);
    }

    public class MyViewHolder extends ViewHolder {
        TextView eventTitle;
        TextView eventDate;
        ImageView eventImageView;
        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            eventTitle = (TextView) itemView.findViewById(R.id.event_title_name);
            eventDate = (TextView) itemView.findViewById(R.id.start_date);
            eventImageView = (ImageView) itemView.findViewById(R.id.event_image);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll_event);
        }


    }


}
