package com.myriadmobile.fargoeventsapp.eventsList.view;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.myriadmobile.fargoeventsapp.R;
import com.myriadmobile.fargoeventsapp.base.BaseFragment;
import com.myriadmobile.fargoeventsapp.eventProfile.view.EventProfileFragment;
import com.myriadmobile.fargoeventsapp.eventsList.model.EvenListUserModel;
import com.myriadmobile.fargoeventsapp.eventsList.presenter.EventListImp;
import com.myriadmobile.fargoeventsapp.eventsList.presenter.EventListPresenter;
import com.myriadmobile.fargoeventsapp.utils.Constant;

import java.util.ArrayList;

import butterknife.BindView;


public class EventListFragment extends BaseFragment implements EventListView {
    EventListPresenter eventListPresenter;
    ArrayList<EvenListUserModel> evenListUserModelArrayList;
    @BindView(R.id.recyclerView)
    RecyclerView tableRecyclerView;


    @Override
    protected int getLayout() {
        return R.layout.fragment_event_list;
    }

    @Override
    protected void init(View view) {
        eventListPresenter = new EventListImp(this, getActivity());
        EvenListUserModel evenListUserModel = new EvenListUserModel();
        eventListPresenter.getEventData(evenListUserModel);

    }


    @Override
    public void setEventListModel(final ArrayList<EvenListUserModel> evenListUserModels) {
        this.evenListUserModelArrayList = evenListUserModels;
        Log.d(Constant.USERCODE, evenListUserModelArrayList.size() + "");
        EventListRecyclerViewAdapter eventListRecyclerViewAdapter = new EventListRecyclerViewAdapter(getActivity(), evenListUserModelArrayList, new EventListRecyclerViewAdapter.CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                EventProfileFragment eventProfileFragment = new EventProfileFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                final Bundle bundle = new Bundle();
                bundle.putString(Constant.EVENTID, evenListUserModels.get(position).getId().toString());
                eventProfileFragment.setArguments(bundle);
                transaction.hide(getFragmentManager().findFragmentByTag("profile"));

                transaction.add(R.id.main_activity_content_frame, eventProfileFragment);
                transaction.addToBackStack(null);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

                transaction.commit();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        tableRecyclerView.setAdapter(eventListRecyclerViewAdapter);
        tableRecyclerView.setLayoutManager(linearLayoutManager);
        eventListRecyclerViewAdapter.notifyDataSetChanged();



    }


}
