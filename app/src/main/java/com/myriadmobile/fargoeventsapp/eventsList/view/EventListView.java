package com.myriadmobile.fargoeventsapp.eventsList.view;

import com.myriadmobile.fargoeventsapp.eventsList.model.EvenListUserModel;

import java.util.ArrayList;

public interface EventListView {


    void setEventListModel(ArrayList<EvenListUserModel> evenListUserModels);

}
