package com.myriadmobile.fargoeventsapp.eventsList.presenter;

import com.myriadmobile.fargoeventsapp.eventsList.model.EvenListUserModel;

public interface EventListPresenter {

    void getEventData(EvenListUserModel evenListUserModel);
}
