package com.myriadmobile.fargoeventsapp.eventsList.presenter;

import android.content.Context;
import android.util.Log;

import com.myriadmobile.fargoeventsapp.api.ApiInterface;
import com.myriadmobile.fargoeventsapp.eventsList.model.EvenListUserModel;
import com.myriadmobile.fargoeventsapp.eventsList.view.EventListFragment;
import com.myriadmobile.fargoeventsapp.eventsList.view.EventListView;
import com.myriadmobile.fargoeventsapp.helper.NoInternetConnectionHelper;
import com.myriadmobile.fargoeventsapp.helper.SharedPreferencesHelper;
import com.myriadmobile.fargoeventsapp.utils.SetupRetrofit;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class EventListImp implements EventListPresenter {
    Context context;
    EventListFragment eventListFragment;
    EventListView eventListView;
    SharedPreferencesHelper sharedPreferencesHelper;
    private ArrayList<EvenListUserModel> evenListUserModels;

    public EventListImp(EventListView eventListView, Context context) {
        this.context = context;
        this.eventListView = eventListView;

    }

    @Override
    public void getEventData(EvenListUserModel evenListUserModel) {

        sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(context);
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(sharedPreferencesHelper.getLoginPreferecnes());

        Log.d("CheckingEventData", "");
        ApiInterface eventApiInterface = retrofit.create(ApiInterface.class);

        Call<ArrayList<EvenListUserModel>> apiInterfaceEventData = eventApiInterface.getEventData();
        apiInterfaceEventData.enqueue(new Callback<ArrayList<EvenListUserModel>>() {
            @Override
            public void onResponse(Call<ArrayList<EvenListUserModel>> call, Response<ArrayList<EvenListUserModel>> response) {

                if (response.isSuccessful()) {
                    evenListUserModels = response.body();
                    eventListView.setEventListModel(evenListUserModels);

                } else {
                }
            }

            @Override
            public void onFailure(Call<ArrayList<EvenListUserModel>> call, Throwable t) {
                t.printStackTrace();

                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }



}
