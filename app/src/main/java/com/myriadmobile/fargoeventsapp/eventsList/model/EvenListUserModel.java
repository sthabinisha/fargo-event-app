package com.myriadmobile.fargoeventsapp.eventsList.model;

import com.google.gson.annotations.SerializedName;


public class EvenListUserModel {


    @SerializedName("id")
    Integer id;
    @SerializedName("title")
    String title;
    @SerializedName("image_url")
    String image_url;
    @SerializedName("start_date_time")
    String start_date_time;
    @SerializedName("end_date_time")
    String end_date_time;
    @SerializedName("location")
    String location;
    @SerializedName("featured")
    Boolean featured;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getStart_date_time() {
        return start_date_time;
    }

    public void setStart_date_time(String start_date_time) {
        this.start_date_time = start_date_time;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }
}
