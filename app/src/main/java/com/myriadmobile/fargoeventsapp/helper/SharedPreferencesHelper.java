package com.myriadmobile.fargoeventsapp.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.myriadmobile.fargoeventsapp.utils.Constant;


public class SharedPreferencesHelper {
    public static String LOGIN = "login";
    public static SharedPreferencesHelper sharedInstance;
    Context context;
    String TOKEN;


    public SharedPreferencesHelper(Context context) {
        this.context = context;
    }

    public static SharedPreferencesHelper getSharedInstance(Context context) {

        if (sharedInstance == null) {
            sharedInstance = new SharedPreferencesHelper(context);
        }
        return sharedInstance;
    }


    public void saveUsernamePassword(String usernameString, String passwordString) {
        SharedPreferences.Editor editor = context.getSharedPreferences(LOGIN, context.MODE_PRIVATE).edit();
        editor.putString(Constant.USERNAME, usernameString);
        editor.putString(Constant.PASSWORD, passwordString);
        editor.commit();
    }

    public String getLoginPreferecnes() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(LOGIN, context.MODE_PRIVATE);
        TOKEN = sharedPreferences.getString(Constant.TOKEN, "");
        return TOKEN;
    }

    public void saveLoginPreferences(String token) {
        SharedPreferences.Editor editor = context.getSharedPreferences(LOGIN, context.MODE_PRIVATE).edit();
        editor.putString(Constant.TOKEN, token);
        editor.commit();
    }

    public void clearLoginPreferences() {
        SharedPreferences.Editor editor = context.getSharedPreferences(LOGIN, context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        Constant.ACESS_TOKEN = "";
    }
}
