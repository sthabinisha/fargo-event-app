package com.myriadmobile.fargoeventsapp.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.myriadmobile.fargoeventsapp.utils.NoInternetClass;

public class NoInternetConnectionHelper {


    public static void showDialog(final Context context) {

        Intent i = new Intent(context, NoInternetClass.class);
        context.startActivity(i);
        ((Activity) context).finish();
    }
}
