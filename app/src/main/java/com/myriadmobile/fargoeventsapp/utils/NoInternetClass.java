package com.myriadmobile.fargoeventsapp.utils;


import com.myriadmobile.fargoeventsapp.R;
import com.myriadmobile.fargoeventsapp.base.BaseActivity;


public class NoInternetClass extends BaseActivity {
    @Override
    protected int getLayout() {
        return R.layout.no_internt;
    }

    @Override
    protected void init() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
