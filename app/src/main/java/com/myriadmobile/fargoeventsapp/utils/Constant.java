package com.myriadmobile.fargoeventsapp.utils;

public class Constant {

    public static final CharSequence REQUIRED ="Required" ;
    public static final String EVENTID = "event_id";
    public static final String EVENTS = "Events";
    public static String USERNAME = "username";
    public static String PASSWORD = "password";

    public static String TOKEN = "token";
    public static String USERCODE = "";
    public static String ACESS_TOKEN = "";
    public static String BASEURL="https://challenge.myriadapps.com/";
    public static String AUTHORIZATION="authorization";
    public static String FORGET="Its Okay, you can use your name!";



}
