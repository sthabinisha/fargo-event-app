package com.myriadmobile.fargoeventsapp.login.model;

import com.google.gson.annotations.SerializedName;


public class LoginUserModel {

    @SerializedName("Username")
    String username;
    @SerializedName("Password")
    String password;


    public LoginUserModel(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
