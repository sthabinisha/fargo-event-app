package com.myriadmobile.fargoeventsapp.login.view;

/**
 * Created by Own on 10/16/2017.
 */

public interface LoginView {

    void failedLogin();
    void userToken(String loginToken);
}
