package com.myriadmobile.fargoeventsapp.login.presenter;


import com.myriadmobile.fargoeventsapp.login.model.LoginUserModel;


public interface LoginPresenter {
    void loginUser(LoginUserModel loginUserModel);
}
