package com.myriadmobile.fargoeventsapp.login.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.myriadmobile.fargoeventsapp.api.ApiInterface;
import com.myriadmobile.fargoeventsapp.login.model.LoginUserModel;
import com.myriadmobile.fargoeventsapp.login.model.UserResponseModel;
import com.myriadmobile.fargoeventsapp.login.view.LoginActivity;
import com.myriadmobile.fargoeventsapp.login.view.LoginView;
import com.myriadmobile.fargoeventsapp.utils.Constant;
import com.myriadmobile.fargoeventsapp.utils.SetupRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class LoginImp implements LoginPresenter {

    LoginView loginView;
    Context context;
    SharedPreferences sharedPreferencesToken;
    UserResponseModel userResponseModel;

    public LoginImp(LoginActivity loginView, Context context) {
        this.loginView = loginView;
        this.context = context;
    }

    @Override
    public void loginUser(LoginUserModel loginUserModel) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        userResponseModel = new UserResponseModel();
        Log.d("CheckingLoginResponse", "");
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<UserResponseModel> getLogin = loginRegInterface.getUserData(loginUserModel);
        getLogin.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {
                Log.d("checkLoginResponse", response.code() + "  ");

                if (response.isSuccessful()) {
                    try {
                        userResponseModel= response.body();
                        loginView.userToken(userResponseModel.getToken());
                        Constant.ACESS_TOKEN = userResponseModel.getToken();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                } else {
                    try {

                        loginView.failedLogin();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {
                t.printStackTrace();
            }


        });
    }


}

