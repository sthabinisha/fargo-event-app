package com.myriadmobile.fargoeventsapp.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.myriadmobile.fargoeventsapp.R;
import com.myriadmobile.fargoeventsapp.activities.MainActivity;
import com.myriadmobile.fargoeventsapp.base.BaseActivity;
import com.myriadmobile.fargoeventsapp.helper.SharedPreferencesHelper;
import com.myriadmobile.fargoeventsapp.login.model.LoginUserModel;
import com.myriadmobile.fargoeventsapp.login.presenter.LoginImp;
import com.myriadmobile.fargoeventsapp.login.presenter.LoginPresenter;
import com.myriadmobile.fargoeventsapp.utils.Constant;

import butterknife.BindView;
import butterknife.OnClick;


public class LoginActivity extends BaseActivity implements LoginView {

    public static final String TAG = "LoginActivity";
    LoginPresenter loginPresenter;
    @BindView(R.id.et_email_address)
    EditText username;
    @BindView(R.id.et_password)
    EditText password;
    @BindView(R.id.btn_login)
    Button login;
    @BindView(R.id.mb_forgot)
    TextView forgotPassword;
    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinatorLayout;
    SharedPreferencesHelper sharedPreferencesHelper;
    String name;


    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void init() {
        loginPresenter = new LoginImp(this, this);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

    }

    @OnClick(R.id.btn_login)
    public void loginClick() {
        String usernameString = username.getText().toString();
        String passwordString = password.getText().toString();

        Constant.ACESS_TOKEN = usernameString;
        Constant.USERNAME = usernameString;
        Constant.PASSWORD = passwordString;
        if (usernameString.length() == 0) {
            username.setError(Constant.REQUIRED);
        } else if (passwordString.length() == 0) {
            password.setError(Constant.REQUIRED);
        } else {
            sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(this);
            sharedPreferencesHelper.saveUsernamePassword(usernameString, passwordString);

            LoginUserModel loginUserModel = new LoginUserModel(usernameString, passwordString);
            loginPresenter.loginUser(loginUserModel);
        }
    }


    @OnClick(R.id.mb_forgot)
    public void setForgotPassword() {
        hideSoftKeyboard();
        Snackbar snackbar = Snackbar.make(coordinatorLayout, Constant.FORGET, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void failedLogin() {

    }


    @Override
    public void userToken(String loginToken) {
        sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(this);
        sharedPreferencesHelper.saveLoginPreferences(loginToken);
        Constant.ACESS_TOKEN = sharedPreferencesHelper.getLoginPreferecnes();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();

    }
}
