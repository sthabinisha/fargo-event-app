package com.myriadmobile.fargoeventsapp.eventProfile.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myriadmobile.fargoeventsapp.R;
import com.myriadmobile.fargoeventsapp.eventProfile.model.EventSpeakerUserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.support.v7.widget.RecyclerView.Adapter;
import static android.support.v7.widget.RecyclerView.ViewHolder;

public class EventSpeakerRecyclerAdapter extends Adapter<EventSpeakerRecyclerAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<EventSpeakerUserModel> eventSpeakerUserModels = new ArrayList<>();
    public EventSpeakerRecyclerAdapter(Context mContext, ArrayList<EventSpeakerUserModel> eventSpeakerUserModel) {
        this.mContext = mContext;
        this.eventSpeakerUserModels = eventSpeakerUserModel;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_speaker_list, parent, false);
        final MyViewHolder mViewHolder = new MyViewHolder(mView);
        return mViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        EventSpeakerUserModel eventSpeakerUserModel = eventSpeakerUserModels.get(position);
        holder.first_name.setText(eventSpeakerUserModel.getFirst_name() + " " + eventSpeakerUserModel.getLast_name());
        holder.bio.setText(eventSpeakerUserModel.getBio());
        try {
            Picasso.with(mContext)
                    .load(eventSpeakerUserModel.getImage_url())
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder)
                    .into(holder.eventImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return eventSpeakerUserModels.size();
    }

    public class MyViewHolder extends ViewHolder {
        TextView first_name;
        TextView bio;
        CircleImageView eventImageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            first_name = (TextView) itemView.findViewById(R.id.speaker_name);
            bio = (TextView) itemView.findViewById(R.id.speaker_description);
            eventImageView = (CircleImageView) itemView.findViewById(R.id.speaker_image);
        }


    }
}
