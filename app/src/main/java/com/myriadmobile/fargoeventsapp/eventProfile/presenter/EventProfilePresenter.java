package com.myriadmobile.fargoeventsapp.eventProfile.presenter;

public interface EventProfilePresenter {

    void getEventProfile(String event_id);

}
