package com.myriadmobile.fargoeventsapp.eventProfile.model;

import com.google.gson.annotations.SerializedName;

public class EventSpeakerUserModel {

    @SerializedName("id")
    Integer id;
    @SerializedName("first_name")

    String first_name;
    @SerializedName("image_url")
    String image_url;
    @SerializedName("bio")
    String bio;
    @SerializedName("last_name")
    String last_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

}
