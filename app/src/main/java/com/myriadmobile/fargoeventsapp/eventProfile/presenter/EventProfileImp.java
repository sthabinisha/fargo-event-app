package com.myriadmobile.fargoeventsapp.eventProfile.presenter;

import android.content.Context;
import android.util.Log;

import com.myriadmobile.fargoeventsapp.api.ApiInterface;
import com.myriadmobile.fargoeventsapp.eventProfile.model.EvenProfileUserModel;
import com.myriadmobile.fargoeventsapp.eventProfile.model.EventSpeakerUserModel;
import com.myriadmobile.fargoeventsapp.eventProfile.view.EventProfileView;
import com.myriadmobile.fargoeventsapp.helper.NoInternetConnectionHelper;
import com.myriadmobile.fargoeventsapp.helper.SharedPreferencesHelper;
import com.myriadmobile.fargoeventsapp.utils.SetupRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EventProfileImp implements EventProfilePresenter {
    SharedPreferencesHelper sharedPreferencesHelper;
    EventProfileView eventProfileView;
    Context context;
    EvenProfileUserModel evenProfileUserModel;
    EventSpeakerUserModel eventSpeakerUserModel;
    ArrayList<EventSpeakerUserModel> eventSpeakerUserModelsList = new ArrayList<>();


    public EventProfileImp(EventProfileView eventProfileView, Context context) {
        this.context = context;
        this.eventProfileView = eventProfileView;

    }


    @Override
    public void getEventProfile(final String event_id) {

        sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(context);
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(sharedPreferencesHelper.getLoginPreferecnes());
        evenProfileUserModel = new EvenProfileUserModel();
        Log.d("CheckingEventData", "");
        ApiInterface eventProfileInterface = retrofit.create(ApiInterface.class);
        Call<EvenProfileUserModel> eventProfileInterfaceEventProfile = eventProfileInterface.getEventProfile(event_id);
        eventProfileInterfaceEventProfile.enqueue(new Callback<EvenProfileUserModel>() {
            @Override
            public void onResponse(Call<EvenProfileUserModel> call, Response<EvenProfileUserModel> response) {
                evenProfileUserModel = response.body();
                eventProfileView.updateProfile(evenProfileUserModel);
                List<EvenProfileUserModel.EventSpeakersUserModel> speakersUserModelList = evenProfileUserModel.speakers;

                for (EvenProfileUserModel.EventSpeakersUserModel speakersUserModel : speakersUserModelList) {

                    getSpeakers(speakersUserModel.getId().toString());
                }
            }

            @Override
            public void onFailure(Call<EvenProfileUserModel> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });

    }

    private void getSpeakers(String id) {
        sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(context);
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(sharedPreferencesHelper.getLoginPreferecnes());
        eventSpeakerUserModel = new EventSpeakerUserModel();
        Log.d("CheckingEventData", "");
        ApiInterface eventProfileInterface = retrofit.create(ApiInterface.class);
        Call<EventSpeakerUserModel> eventProfileInterfaceEventProfile = eventProfileInterface.getSpeakers(id);
        eventProfileInterfaceEventProfile.enqueue(new Callback<EventSpeakerUserModel>() {
            @Override
            public void onResponse(Call<EventSpeakerUserModel> call, Response<EventSpeakerUserModel> response) {
                eventSpeakerUserModel = response.body();
                eventSpeakerUserModelsList.add(eventSpeakerUserModel);
                eventProfileView.updateSpeaker(eventSpeakerUserModelsList);

            }

            @Override
            public void onFailure(Call<EventSpeakerUserModel> call, Throwable t) {

                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });

    }

}
