package com.myriadmobile.fargoeventsapp.eventProfile.view;

import com.myriadmobile.fargoeventsapp.eventProfile.model.EvenProfileUserModel;
import com.myriadmobile.fargoeventsapp.eventProfile.model.EventSpeakerUserModel;

import java.util.ArrayList;

public interface EventProfileView {


    void updateProfile(EvenProfileUserModel evenProfileUserModel);

    void updateSpeaker(ArrayList<EventSpeakerUserModel> eventSpeakerUserModel);
}
