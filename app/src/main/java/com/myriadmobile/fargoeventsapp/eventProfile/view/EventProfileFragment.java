package com.myriadmobile.fargoeventsapp.eventProfile.view;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.myriadmobile.fargoeventsapp.R;
import com.myriadmobile.fargoeventsapp.base.BaseFragment;
import com.myriadmobile.fargoeventsapp.eventProfile.model.EvenProfileUserModel;
import com.myriadmobile.fargoeventsapp.eventProfile.model.EventSpeakerUserModel;
import com.myriadmobile.fargoeventsapp.eventProfile.presenter.EventProfileImp;
import com.myriadmobile.fargoeventsapp.eventProfile.presenter.EventProfilePresenter;
import com.myriadmobile.fargoeventsapp.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;


public class EventProfileFragment extends BaseFragment implements EventProfileView {
    EventProfilePresenter eventProfilePresenter;
    String event_id;
    EvenProfileUserModel evenProfileUserModel;
    @BindView(R.id.event_image)
    ImageView event_image;
    @BindView(R.id.event_location)
    TextView event_location;
    @BindView(R.id.event_title)
    TextView event_title;
    @BindView(R.id.event_time)
    TextView event_time;
    @Nullable
    @BindView(R.id.event_description)
    TextView event_description;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.arrow)
    TextView arrow;

    @Override
    protected int getLayout() {
        return R.layout.fragment_event_profile;
    }

    @Override
    protected void init(View view) {
        evenProfileUserModel = new EvenProfileUserModel();
        eventProfilePresenter = new EventProfileImp(this, getActivity());
        event_id = getArguments().getString(Constant.EVENTID);
        eventProfilePresenter.getEventProfile(event_id);

    }




    @Override
    public void updateProfile(final EvenProfileUserModel evenProfileUserModel) {
        event_title.setText(evenProfileUserModel.getTitle());
        event_time.setText(evenProfileUserModel.getEnd_date_time());
        event_description.setText(evenProfileUserModel.getEvent_description());
        event_location.setText(evenProfileUserModel.getLocation());
        event_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+evenProfileUserModel.getLocation());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        try {
            Picasso.with(getActivity())
                    .load(evenProfileUserModel.getImage_url())
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder)
                    .tag("Profile")
                    .into(event_image);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateSpeaker(ArrayList<EventSpeakerUserModel> eventSpeakerUserModel) {
        if(eventSpeakerUserModel.size()>=2){
            arrow.setVisibility(View.VISIBLE);
        }
        EventSpeakerRecyclerAdapter eventSpeakerRecyclerAdapter = new EventSpeakerRecyclerAdapter(getActivity(), eventSpeakerUserModel);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setAdapter(eventSpeakerRecyclerAdapter);
        recyclerView.setLayoutManager(layoutManager);
        eventSpeakerRecyclerAdapter.notifyDataSetChanged();

    }


}
